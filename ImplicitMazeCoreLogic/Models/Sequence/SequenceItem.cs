﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImplicitMazeCoreLogic.Models.Sequence
{
    public class SequenceItem : SequenceItemRaw
    {
        public bool IsCompleted { get; set; }

        public SequenceItem(SequenceItemRaw sequenceItemRaw) : base(sequenceItemRaw.SequenceItemTypeEnum, sequenceItemRaw.IsItemMeasured)
        {
            this.IsCompleted = false;
        }
    }
}
