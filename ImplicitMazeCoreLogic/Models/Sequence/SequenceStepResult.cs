﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImplicitMazeCoreLogic.Models.Sequence
{
    public class SequenceStepResult
    {
        public readonly List<SequenceItem> SequenceItems;
        public readonly int CompletedSequenceCount;
        public readonly string Message;

        public SequenceStepResult(List<SequenceItem> sequenceItems, int completedSequenceCount, string message)
        {
            this.SequenceItems = sequenceItems;
            this.CompletedSequenceCount = completedSequenceCount;
            this.Message = message;
        }
    }
}
