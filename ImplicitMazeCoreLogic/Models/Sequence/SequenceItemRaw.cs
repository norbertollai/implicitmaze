﻿using ImplicitMazeCoreLogic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImplicitMazeCoreLogic.Models.Sequence
{
    public class SequenceItemRaw
    {
        public readonly SequenceItemTypeEnum SequenceItemTypeEnum;
        public readonly bool IsItemMeasured;

        public SequenceItemRaw(SequenceItemTypeEnum sequenceItemTypeEnum, bool isItemMeasured)
        {
            this.SequenceItemTypeEnum = sequenceItemTypeEnum;
            this.IsItemMeasured = isItemMeasured;
        }
    }
}
