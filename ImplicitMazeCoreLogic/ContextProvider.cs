﻿using ImplicitMazeCoreLogic.Controllers;

namespace ImplicitMazeCoreLogic
{
    public static class ContextProvider
    {
        private static Controller controller;

        static ContextProvider()
        {
            controller = new Controller();
        }

        public static Controller GetController()
        {
            return controller;
        }
    }
}
