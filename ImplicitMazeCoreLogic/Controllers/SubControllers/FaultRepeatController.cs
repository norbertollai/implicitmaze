﻿using ImplicitMazeCoreLogic.Types;

namespace ImplicitMazeCoreLogic.Controllers.SubControllers
{
    internal class FaultRepeatController
    {
        public bool IsRepeatEnabled { get; private set; }

        internal FaultRepeatController()
        {
            this.IsRepeatEnabled = false;
        }

        internal FaultRepeatController(bool isRepeatEnabled)
        {
            this.IsRepeatEnabled = isRepeatEnabled;
        }

        public bool SetFaultRepeat(FaultRepeatActionEnum faultRepeatActionEnum)
        {
            switch(faultRepeatActionEnum)
            {
                case FaultRepeatActionEnum.Switch:
                    repeatSwitch();
                    break;
                case FaultRepeatActionEnum.Enable:
                    repeatEnable();
                    break;
                case FaultRepeatActionEnum.Disable:
                    repeatDisable();
                    break;
            }

            return this.IsRepeatEnabled;
        }

        private void repeatEnable()
        {
            this.IsRepeatEnabled = true;
        }

        private void repeatDisable()
        {
            this.IsRepeatEnabled = false;       
        }

        private void repeatSwitch()
        {
            this.IsRepeatEnabled = !this.IsRepeatEnabled;
        }
    }
}
