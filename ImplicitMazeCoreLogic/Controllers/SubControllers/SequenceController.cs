﻿using ImplicitMazeCoreLogic.Models;
using ImplicitMazeCoreLogic.Models.Sequence;
using ImplicitMazeCoreLogic.Types;
using System.Collections.Generic;

namespace ImplicitMazeCoreLogic.Controllers.SubControllers
{
    internal class SequenceController
    {
        public bool IsFaultRepeat { private get; set; }
        public int CompletedSequenceCount { get; private set; }
        private readonly List<SequenceItemRaw> DUMMY_ITEMS = new List<SequenceItemRaw>() {
            new SequenceItemRaw(SequenceItemTypeEnum.BlueDoor, false),
            new SequenceItemRaw(SequenceItemTypeEnum.RedDoor, true)
        };
        private List<SequenceItem> sequenceItems;
        private int actualSequenceIndex;

        internal SequenceController()
        {
            this.sequenceItems = this.createSequenceListFrom(DUMMY_ITEMS);
        }

        internal SequenceController(List<SequenceItemRaw> sequenceItemRaws)
        {
            this.IsFaultRepeat = false;         
            this.CompletedSequenceCount = 0;
            this.sequenceItems = this.createSequenceListFrom(sequenceItemRaws);
            this.actualSequenceIndex = 0;
        }

        public SequenceStepResult MakeStep(SequenceItemTypeEnum sequenceItemTypeEnum)
        {
            string message = "";
            SequenceItem actualSequenceItem = this.sequenceItems[this.actualSequenceIndex];
            if (actualSequenceItem.IsItemMeasured)
            {
                if (actualSequenceItem.SequenceItemTypeEnum.Equals(sequenceItemTypeEnum))
                {
                    actualSequenceItem.IsCompleted = true;
                    this.stepTheActualSequenceIndex();
                }
                else
                {
                    if (!this.IsFaultRepeat)
                    {
                        this.resetStreak();
                        this.stepTheActualSequenceIndex();
                    }
                }
            }
            else
            {
                this.stepTheActualSequenceIndex();
            }

            return new SequenceStepResult(this.sequenceItems, this.CompletedSequenceCount, message);
        }

        private List<SequenceItem> createSequenceListFrom(List<SequenceItemRaw> sequenceItems)
        {
            List<SequenceItem> result  = new List<SequenceItem>();
            foreach (SequenceItemRaw item in sequenceItems)
            {
                result.Add(new SequenceItem(item));
            }
            return result;
        }

        private void stepTheActualSequenceIndex()
        {
            this.actualSequenceIndex = (this.actualSequenceIndex == (sequenceItems.Count - 1)) ? 0 : ++this.actualSequenceIndex;
        }

        private void resetStreak()
        {
            this.sequenceItems.ForEach(item => { if (item.IsItemMeasured) item.IsCompleted = false; });
        }
    }
}
