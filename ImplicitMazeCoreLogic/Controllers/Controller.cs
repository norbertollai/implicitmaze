﻿using ImplicitMazeCoreLogic.Controllers.SubControllers;
using ImplicitMazeCoreLogic.Models;
using ImplicitMazeCoreLogic.Models.Sequence;
using ImplicitMazeCoreLogic.Types;
using System.Collections.Generic;

namespace ImplicitMazeCoreLogic.Controllers
{
    public class Controller
    {
        private FaultRepeatController faultRepeatController;
        private SequenceController sequenceController;

        internal Controller()
        {
            this.faultRepeatController = new FaultRepeatController();
            this.sequenceController = new SequenceController();
        }

        public void Setup(bool isRepeatEnabled, List<SequenceItemRaw> sequenceItems)
        {
            this.faultRepeatController = new FaultRepeatController(isRepeatEnabled);
            this.sequenceController = new SequenceController(sequenceItems);
        }

        public bool SetFaultRepeat(FaultRepeatActionEnum faultRepeatActionEnum)
        {
            return this.faultRepeatController.SetFaultRepeat(faultRepeatActionEnum);
        }

        public SequenceStepResult MakeStep(SequenceItemTypeEnum sequenceItemValueEnum)
        {
            return this.sequenceController.MakeStep(sequenceItemValueEnum);
        }
    }
}
