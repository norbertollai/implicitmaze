﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImplicitMazeCoreLogic.Types
{
    public enum FaultRepeatActionEnum
    {
        Switch,
        Enable,
        Disable
    }
}
